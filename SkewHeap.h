//
// Created by Brett Spackman on 3/22/18.
//

#ifndef CSHW5_SKEWHEAP_H
#define CSHW5_SKEWHEAP_H

#include <string>
#include "Node.h"
#include "ItemType.h"

class SkewHeap {
public:
  void insert(Node * node);
  void insert(ItemType);
  Node * deleteMin();
  void printTree() { printTree(root, "");}

private:
  Node * root;
  Node * SkewHeapMerge(Node * H1, Node * H2);
  void swapChildren (Node * swapHead);
  void printTree(Node* start, std::string indent);

};

#endif //CSHW5_SKEWHEAP_H
