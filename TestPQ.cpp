#include "PQHeap.h"
#include "SkewHeap.h"
#include <time.h>
//#include "SkewHeap.h"
//#include "Leftist.h"

//Insert limit elements of the file fin into heap.
void insertNext(PQ & pq,std::ifstream & fin,int limit=0) 
{	if (limit ==0) 
		limit = std::numeric_limits<int>::max();
	std::string word;
	int ct;
	for (int i =0; i <= limit && !fin.eof(); i++)
	{
		fin >> word >> ct;
		//std::cout << "inserting " << word << ct << std::endl;
		ItemType item(word, ct);
		pq.insert(item);
	}
}

void findMedian(std::ifstream & fin) {
  std::string shWord;
  int shCount;
  ItemType newValue;
  ItemType currMedian;
  int maxSize = 0;
  int minSize = 0;
  SkewHeap minHeap;
  PQHeap maxHeap("For Median", 6000);

  while (fin >> shWord >> shCount)
  {
    newValue.word = shWord;
    newValue.priority = shCount;
    if (currMedian.word == "" && currMedian.priority == 0)
    {
      currMedian = newValue;
      continue;
    }
    if (newValue.priority > currMedian.priority)
    {
      minHeap.insert(newValue);
      minSize ++;
    }
    else // (newValue < currMedian)
    {
      maxHeap.insert(newValue);
      maxSize ++;
    }

    if (maxSize - minSize > 1)
    {
      ItemType temp = maxHeap.deleteMax();
      minHeap.insert(currMedian);
      currMedian = temp;
      maxSize --;
      minSize ++;
    }

    if (minSize - maxSize > 1)
    {
      Node * temp = minHeap.deleteMin();
      maxHeap.insert(currMedian);
      currMedian = temp->element;
      minSize--;
      maxSize++;
    }

    if ((minSize + maxSize) % 100 == 0)
    {
      std::cout << currMedian.toString() << std::endl;
    }
  }
  std::cout << currMedian.toString();
}

int main()
{
//  clock_t start = clock();
//	int const DELETE_CT=20;
//	int const PRINTSIZE=30;
//	int const HOWMANY = 100;

  // testing max heap

  std::cout << std::endl << "Testing Max Heap by printing out deletions " << std::endl << std::endl;

  PQHeap pqMax("MaxHeap",6000);

    std::ifstream fin;
	fin.open("Prog5In.txt");
	assert(fin);

  std::string shWord;
  int shCount;

  ItemType newData;

  while (fin >> shWord >> shCount)
  {
    newData.word = shWord;
    newData.priority = shCount;

    Node * node = new Node(newData);

    pqMax.insert(newData);

  }

  for (int i = 0; i < 100; i++)
  {
    std::cout << pqMax.deleteMax().toString() << std::endl;
  }

  fin.close();


  // testing for findMedian

  std::cout << std::endl << std::endl <<  "Testing my median " << std::endl << std::endl << std::endl << std::endl;
  fin.open("Prog5In.txt");
  findMedian(fin);
  fin.close();


//
//	for (int i = 0; i < 60; i++)
//	{
//		insertNext(pqMax, fin, HOWMANY);
//		std::cout << pqMax.deleteMax().toString() << std::endl;
//	}
//
//	clock_t time = clock()-start;
//	std::cout << "Elapsed time = " << ((float) time)/CLOCKS_PER_SEC << std::endl;
//	std::cin.ignore();



  // testing for skew heap

  std::cout << std::endl << std::endl << std::endl << "Testing Skew/Min Heap by printing out deletions " << std::endl << std::endl << std::endl << std::endl;

  SkewHeap skewHeap;

  fin.open("Prog5In.txt");

  while (fin >> shWord >> shCount)
  {
    newData.word = shWord;
    newData.priority = shCount;

    Node * node = new Node(newData);

    skewHeap.insert(node);
  }

  // skewHeap.printTree();

  fin.close();

//   for loop checking to see if my deletion is working
//   its got a ton of deletions, too many?
//   also have it printing out deletions in the deleteMin() function

  for (int i = 0; i < 100; i++)
  {
    std::cout << skewHeap.deleteMin()->element.toString() << std::endl;
  }

}


